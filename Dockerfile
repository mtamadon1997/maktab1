FROM bitnami/laravel:8-debian-10

WORKDIR /app

COPY . /app 

RUN composer install

COPY .env.example .env

CMD [ "sh" , "run.sh"]
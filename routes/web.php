<?php

use Illuminate\Support\Facades\Route;
use App\Models\Post;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/posts', function(){
    $posts = Post::all();
    return view('posts',['posts'=>$posts]);
});


Route::post('/posts', function(Request $request){
    $data = $request->all();
    $post = new Post;
    $post->title = $data["title"];
    $post->save();
    return redirect('/posts');
});
